from zipfile import ZipFile
import os
import shutil

EXTRACTED_FILES = []
EXTRACTED_DIRECTORIES = []

def get_all_file_paths(directory):
    # initializing empty file paths list
    file_paths = []

    # crawling through directory and subdirectories
    for root, directories, files in os.walk(directory):
        for filename in files:
            # join the two strings in order to form the full filepath.
            filepath = os.path.join(root, filename)
            file_paths.append(filepath)

            # returning all file paths
    return file_paths

"""
Call this function to zip files
"""
def create_zip():
    shutil.make_archive('../final_extracted', 'zip', '../extracted/')


# def create_zip():
#     # path to folder which needs to be zipped
#     directory = '../extracted/'
#
#     # calling function to get all file paths in the directory
#     file_paths = get_all_file_paths(directory)
#
#     # printing the list of all files to be zipped
#     print('Following files will be zipped:')
#     for file_name in file_paths:
#         print(file_name)
#
#     # writing files to a zipfile
#     with ZipFile('../extracted.zip', 'w') as zip:
#         # writing each file one by one
#         for file in file_paths:
#             zip.write(file)
#
#     print('All files zipped successfully!')

"""
@:param the zip file that you want to extract
"""
def extract_zip(zip_file):
    # specifying the zip file name
    # zip_file = "my_python_files.zip"

    # opening the zip file in READ mode
    # with ZipFile('../' + zip_file, 'r') as zip:
    #     # printing all the contents of the zip file
    #     zip.printdir()
    #
    #     # extracting all the files
    #     print('Extracting all the files now...')
    #     zip.extractall()
    #     print('Done!')

    # opening the zip file in READ mode
    with ZipFile('../' + zip_file, 'r') as zip:
        for zip_info in zip.infolist():
            directory = 'extracted/' + zip_info.filename[:-4]
            os.mkdir('../' + directory)
            EXTRACTED_DIRECTORIES.append('../' + directory + "/")
            EXTRACTED_FILES.append('../' + directory + "/" + zip_info.filename)
            zip.extract(zip_info, '../' + directory)
            print(zip_info)

    print(EXTRACTED_FILES)






