from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
import json
from django.http import HttpResponse, JsonResponse
import slack

from .file_downloader import download_file
from .zip_helper import extract_zip, EXTRACTED_DIRECTORIES, EXTRACTED_FILES, create_zip
from .ocr import text_extractor
from .aws_helper import upload_to_s3, get_file_url

import threading

BUCKET_NAME = 'is238-g8-slackbotbucket'
FINAL_ZIPNAME = 'final_extracted.zip'

def home(request):
    return HttpResponse('<h1>Home Page</h1>')

def process_thread(event_msg, client):
    # this will download file from slack and save it in root folder slackbot test
    download_file(settings.BOT_USER_ACCESS_TOKEN, event_msg['files'][0]['id'])

    client.chat_postMessage(channel=event_msg['channel'], text=":8p: :16p: Downloaded file")

    # this will extract the zip file and save it in separate directories
    extract_zip(event_msg['files'][0]['name'])

    client.chat_postMessage(channel=event_msg['channel'], text=":25p: :33p: :42p: Extracted zip file")

    # this will extract text from each pdf file
    text_extractor(EXTRACTED_FILES, EXTRACTED_DIRECTORIES)

    client.chat_postMessage(channel=event_msg['channel'], text=":50p: :58p: :67p: :75p: Extracted text files")

    # this will create a zip of extracted directories in root
    create_zip()

    client.chat_postMessage(channel=event_msg['channel'], text=":83p: :92p: Zipped file")

    # this will upload the zipped file to s3
    upload_to_s3(BUCKET_NAME, f'../{FINAL_ZIPNAME}', FINAL_ZIPNAME)

    client.chat_postMessage(channel=event_msg['channel'], text=f":white_check_mark: File is now "
                                                             f"available at {get_file_url(BUCKET_NAME, FINAL_ZIPNAME)}")

    print(get_file_url(BUCKET_NAME, FINAL_ZIPNAME))

    # client.reactions_add(channel=channel, name="thumbsup")



""" THIS IS FOR TESTING """
@csrf_exempt
def event_hook(request):
    client = slack.WebClient(token=settings.BOT_USER_ACCESS_TOKEN)
    json_dict = json.loads(request.body.decode('utf-8'))

    if json_dict['token'] != settings.VERIFICATION_TOKEN:
        return HttpResponse(status=403)

    if 'type' in json_dict:
        if json_dict['type'] == 'url_verification':
            response_dict = {"challenge": json_dict['challenge']}
            return JsonResponse(response_dict, safe=False)

    if 'event' in json_dict:
        # print(json.dumps(json_dict, indent=4))

        event_msg = json_dict['event']

        if ('subtype' in event_msg) and (event_msg['subtype'] == 'bot_message'):
            return HttpResponse(status=200)

        # do this only when the user was the last one who sent the message
        #if event_msg['type'] == 'message' and event_msg['user'] == 'U01FF7ENE9J':
		channel = event_msg['channel']

		response_msg = f":loading:, You have uploaded {event_msg['files'][0]['name']}"

		client.chat_postMessage(channel=channel, text=response_msg)
		# client.chat_postMessage(channel=channel, text=str(json.dumps(json_dict, indent=4)))

		th = threading.Thread(target=process_thread, args=(event_msg, client,))
		th.start()

		return HttpResponse(status=200)


        # if event_msg['type'] == 'message' and event_msg['user'] != 'U01F95X6ACC':
        #     channel = event_msg['channel']
        #     time.sleep(3)
        #     client.chat_update(channel=channel, text="message updated")

    return HttpResponse(status=200)


""" THIS IS FOR GROUP 8 WORKSPACE """
# @csrf_exempt
# def event_hook(request):
#     client = slack.WebClient(token=settings.BOT_USER_ACCESS_TOKEN)
#     json_dict = json.loads(request.body.decode('utf-8'))
#
#     if json_dict['token'] != settings.VERIFICATION_TOKEN:
#         return HttpResponse(status=403)
#
#     if 'type' in json_dict:
#         if json_dict['type'] == 'url_verification':
#             response_dict = {"challenge": json_dict['challenge']}
#             return JsonResponse(response_dict, safe=False)
#
#     if 'event' in json_dict:
#         # print(json.dumps(json_dict, indent=4))
#
#         event_msg = json_dict['event']
#         print(event_msg['user'])
#         if ('subtype' in event_msg) and (event_msg['subtype'] == 'bot_message'):
#             return HttpResponse(status=200)
#
#         if event_msg['type'] == 'message' and event_msg['user'] == 'U01FF7ENE9J':
#         # if event_msg['type'] == 'message' and event_msg['user'] == 'U01F95X6ACC':
#             user = event_msg['user']
#             channel = event_msg['channel']
#             # response_msg = ":wave:, Hello <@%s>" % user
#
#             response_msg = f":loading:, You have uploaded {event_msg['files'][0]['name']}"
#
#             # response_msg = f":loading:, You have uploaded {event_msg['files'][0]['name']} which you can download at " \
#             #               f"{event_msg['files'][0]['url_private']}"
#
#             client.chat_postMessage(channel=channel, text=response_msg)
#             # client.chat_postMessage(channel=channel, text=str(json.dumps(json_dict, indent=4)))
#             # client.chat_postMessage(channel=channel, text="Received")
#             return HttpResponse(status=200)
#     return HttpResponse(status=200)