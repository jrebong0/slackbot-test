# Import libraries
from PIL import Image
import pytesseract
import sys
from pdf2image import convert_from_path # download poppler
import os

POPPLER_PATH = r'C:\Users\User\Downloads\software\poppler-0.68.0\bin'
# TESSERACT_CMD = r'C:\Program Files\Tesseract-OCR\tesseract.exe' # windows
TESSERACT_CMD = '/usr/bin/tesseract' # ubuntu

def text_extractor(file_paths, directories):

    for file_no in range(len(file_paths)):
        # Path of the pdf
        PDF_file = file_paths[file_no]

        ''' 
        Part #1 : Converting PDF to images 
        '''

        # Store all the pages of the PDF in a variable
        # pages = convert_from_path(PDF_file, 200, poppler_path=POPPLER_PATH) # Windows
        pages = convert_from_path(PDF_file, 200) # ubuntu

        # Counter to store images of each page of PDF to image
        image_counter = 1

        # create a separate directory to store images for each pdf file
        os.mkdir(f'../images/{file_no}')

        # Iterate through all the pages stored above
        for page in pages:
            # Declaring filename for each page of PDF as JPG
            # For each page, filename will be:
            # PDF page 1 -> page_1.jpg
            # PDF page 2 -> page_2.jpg
            # PDF page 3 -> page_3.jpg
            # ....
            # PDF page n -> page_n.jpg
            filename = str(image_counter) + ".jpg"

            # Save the image of the page in system
            page.save(f'../images/{file_no}/{filename}', 'JPEG')

            # Increment the counter to update filename
            image_counter = image_counter + 1

        '''
        Part #2 - Recognizing text from the images using OCR
        '''

        # Variable to get count of total number of pages
        filelimit = image_counter - 1

        # Creating a text file to write the output
        # outfile = "textfiles/"
        outfile = directories[file_no]

        # Open the file in append mode so that
        # All contents of all images are added to the same file
        # f = open(outfile, "a")

        pytesseract.pytesseract.tesseract_cmd = TESSERACT_CMD

        # Iterate from 1 to total number of pages
        for i in range(1, filelimit + 1):
            f = open(outfile + f"{i}.txt", "w")

            # Set filename to recognize text from
            # Again, these files will be:
            # 1.jpg
            # 2.jpg
            # ....
            # n.jpg
            # filename = "images/" + "page_" + str(i) + ".jpg"
            filename = f"../images/{file_no}/{str(i)}.jpg"

            # Recognize the text as string in image using pytesserct
            text = str(pytesseract.image_to_string(Image.open(filename)))

            # The recognized text is stored in variable text
            # Any string processing may be applied on text
            # Here, basic formatting has been done:
            # In many PDFs, at line ending, if a word can't
            # be written fully, a 'hyphen' is added.
            # The rest of the word is written in the next line
            # Eg: This is a sample text this word here GeeksF-
            # orGeeks is half on first line, remaining on next.
            # To remove this, we replace every '-\n' to ''.
            text = text.replace('-\n', '')

            # Finally, write the processed text to the file.
            f.write(text)

        # Close the file after writing all the text.
        f.close()
        print(f"Finished file {file_no}")

        # removed the pdf file in directory
        os.remove(file_paths[file_no])


