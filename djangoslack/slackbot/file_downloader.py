import requests
import os

def download_file(token, file_id):
    print("File ID: ", file_id)

    # call file info to get url
    url = "https://slack.com/api/files.info"
    r = requests.get(url, {"token": token, "file": file_id})
    r.raise_for_status
    response = r.json()
    assert response["ok"]
    file_name = response["file"]["name"]
    file_url = response["file"]["url_private"]
    print("Downloaded " + file_name)

    # download file
    r = requests.get(file_url, headers={'Authorization': 'Bearer %s' % token})
    r.raise_for_status
    file_data = r.content  # get binary content

    # save file to disk
    with open('../' + file_name, 'w+b') as f:
        f.write(bytearray(file_data))
    print("Saved " + file_name + " in root folder")

    os.mkdir('../images')
    os.mkdir('../extracted')
