from django.urls import path
from .views import event_hook, home

urlpatterns = [
    path('', home, name='home'),
    path('slackbot/', event_hook, name='event_hook'),
]
